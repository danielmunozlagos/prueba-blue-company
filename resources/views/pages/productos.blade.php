@extends('layouts.app', ['activePage' => 'productos', 'titlePage' => __('Productos')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Productos</h4>
          </div>
          <div class="card-body">
              
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>
                    ID
                  </th>
                  <th>
                    Nombre
                  </th>
                  <th>
                    Fecha Expiracion
                  </th>
                  <th>
                    Categoria
                  </th>
                  <th>
                    Valor
                  </th>
                  <th>
                  </th>
                </thead>
                <tbody>
                  @foreach ($productos as $producto)
                  <tr>
                    <td>
                      {{ $producto->id }}
                    </td>
                    <td>
                      {{ $producto->nombre }}
                    </td>
                    <td>
                      {{ $producto->categorias->nombre == 'Alimentos' ? date('d-m-Y', strtotime($producto->fecha_expiracion)) : '' }}
                    </td>
                    <td>
                      {{ $producto->categorias->nombre }}
                    </td>
                    <td>
                      $ {{ number_format($producto->valor, 0, ',','.') }}
                    </td>
                    <td>
                      <button class="btn btn-danger btn-xs" onclick="if (confirm('realmente quieres eliminar el elemento: {{ $producto->id }}')) $('#form-delete-prop-{{$producto->id}}').submit()">Borrar<i class="material-icons">delete_forever</i></button>
                      <form action="{{ route('destroyProducto', $producto->id) }}" id="form-delete-prop-{{$producto->id}}" method="POST" style="display: inline;">
                      @csrf
                    </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
                <tfoot>
                  <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('addProducto') }}">Agregar Producto</a>
                  </div>
                  <form id="formulariobuscarcategoria" action="{{route('productos')}}" method="get">
                    <div class="form-group">
                      <label for="categoriaProductoBuscador">Categoria</label>
                      <select class="form-control" data-style="btn btn-link" id="categoriaProductoBuscador" name="categoriaProductoBuscador" required="">
                          <option value="" selected=""  disabled="">Seleccione...</option>
                        @foreach($categorias as $categoria)
                          <option value="{{$categoria->id}}">{{$categoria->nombre}}</option>
                        @endforeach 
                      </select>
                    </div>
                  </form>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('js')
<script>
  $('#categoriaProductoBuscador').on('change',function(e){
    $('#formulariobuscarcategoria').submit();
  });
</script>
@endpush