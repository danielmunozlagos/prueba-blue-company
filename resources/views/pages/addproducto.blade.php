@extends('layouts.app', ['activePage' => 'productos', 'titlePage' => __('Agregar Producto')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="container-fluid">
      <div class="card card-plain">
        <div class="card-header card-header-primary">
          <h4 class="card-title">Nuevo Producto</h4>
        </div>
        <div class="row">
          @if ($errors->any())
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif
          <div class="col-md-12">
            <div class="card-body">
              <form action="{{ route('saveProducto') }}" method="post">
                @csrf
                <div class="form-group">
                  <label for="nombreProducto">Nombre</label>
                  <input type="text" class="form-control" id="nombreProducto" placeholder="" name="nombre" >
                </div>
                <div class="form-group">

                  <label for="categoriaProducto">Categoria</label>
                  <select class="form-control" data-style="btn btn-link" id="categoriaProducto" name="categoria" required="">
                      <option value="" selected=""  disabled="">Seleccione...</option>
                    @foreach($categorias as $categoria)
                      <option value="{{$categoria->id}}">{{$categoria->nombre}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group" id="divFechaExpiracion" style="display: none;">
                  <label for="fechaExpiracionProducto">Fecha de Expiracion</label>
                  <input type="date" class="form-control" id="fechaExpiracionProducto" name="fecha_expiracion">
                </div>
                <div class="form-group">
                  <label for="valorProducto">Valor</label>
                  <input type="number" class="form-control" id="valorProducto" name="valor" required="" min="1" pattern="^[0-9]+" style="-webkit-appearance: textfield !important; margin: 0; -moz-appearance:textfield !important;">
                </div>
                <div class="pull-right"><button type="submit" class="btn btn-success">Success</button></div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('js')
<script>
  $('#categoriaProducto').on('change',function(e){
    if($("#categoriaProducto option:selected").text() !== 'Alimentos'){
      $('#divFechaExpiracion').hide();
      $('#fechaExpiracionProducto').attr('required', false);
    }else{
      $('#divFechaExpiracion').show();
      $('#fechaExpiracionProducto').attr('required', true);
    }

  });
</script>
@endpush