<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
	protected $table = 'productos';
	protected $primaryKey = 'id';
	protected $fillable = ['id_categoria', 'nombre', 'valor', 'fecha_expiracion'];
	public $timestamps = true;

	protected $with = 'Categorias';


	public function Categorias() { return $this->belongsTo('App\Models\Categorias', 'id_categoria', 'id'); }
}
