<?php

namespace App\Http\Controllers;

use App\Models\Productos;
use App\Models\Categorias;
use Illuminate\Http\Request;

class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (isset($request->categoriaProductoBuscador)) {
            $producto = Productos::where('id_categoria', $request->categoriaProductoBuscador)->orderBy('id', 'desc')->get();
        }else{
            $producto = Productos::orderBy('id', 'desc')->get();
        }
        $categorias = Categorias::all();
        return view('pages.productos', ['productos' => $producto, 'categorias' => $categorias]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = Categorias::all();
        return view('pages.addproducto', ['categorias' => $categorias]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required',
            'categoria' => 'required',
            'valor' => 'required',
        ]);

        try {
            $producto = new Productos;
            $producto->nombre = $request->get('nombre');
            $producto->id_categoria = $request->get('categoria');
            $producto->fecha_expiracion = $request->get('fecha_expiracion');
            $producto->valor = $request->get('valor');
            $producto->save();
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }

        return redirect()->route('productos');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function show(Productos $productos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        // 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $producto = Productos::find($request->id);
            $producto->delete();
            return redirect()->route('productos');
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
            return redirect()->route('productos');
        }
    }
}
