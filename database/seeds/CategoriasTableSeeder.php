<?php

use Illuminate\Database\Seeder;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categorias = ['Aseo', 'Alimentos', 'Hogar', 'Mascotas', 'Bebestibles', 'Congelados', 'Libreria'];

        foreach ($categorias as $key => $value) {
        	DB::table('categorias')->insert([
				'nombre' => $value,
	        ]);
        }
    }
}
