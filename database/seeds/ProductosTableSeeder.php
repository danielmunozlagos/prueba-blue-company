<?php

use Illuminate\Database\Seeder;

class ProductosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $productos = ['Té Dharamsala', 'Cerveza tibetana Barley', 'Sirope de regaliz', 'Especias Cajun del chef Anton', 'Mezcla Gumbo del chef Anton', 'Mermelada de grosellas de la abuela', 'Peras secas orgánicas del tío Bob', 'Salsa de arándanos Northwoods', 'Buey Mishi Kobe', 'Pez espada', 'Queso Cabrales', 'Queso Manchego La Pastora', 'Algas Konbu', 'Cuajada de judías', 'Salsa de soja baja en sodio', 'Postre de merengue Pavlova', 'Cordero Alice Springs', 'Langostinos tigre Carnarvon', 'Pastas de té de chocolate', 'Mermelada de Sir Rodneys', 'Bollos de Sir Rodneys', 'Pan de centeno crujiente estilo Gustafs', 'Pan fino', 'Refresco Guaraná Fantástica', 'Crema de chocolate y nueces NuNuCa', 'Ositos de goma Gumbär', 'Chocolate Schoggi', 'Col fermentada Rössle', 'Salchicha Thüringer', 'Arenque blanco del noroeste', 'Queso gorgonzola Telino', 'Queso Mascarpone Fabioli', 'Queso de cabra', 'Cerveza Sasquatch', 'Cerveza negra Steeleye', 'Escabeche de arenque', 'Salmón ahumado Gravad', 'Vino Côte de Blaye', 'Licor verde Chartreuse', 'Carne de cangrejo de Boston', 'Crema de almejas estilo Nueva Inglaterra', 'Tallarines de Singapur', 'Café de Malasia', 'Azúcar negra Malacca', 'Arenque ahumado', 'Arenque salado', 'Galletas Zaanse', 'Chocolate holandés', 'Regaliz', 'Chocolate blanco', 'Manzanas secas Manjimup', 'Cereales para Filo', 'Empanada de carne', 'Empanada de cerdo', 'Paté chino', 'Gnocchi de la abuela Alicia', 'Raviolis Angelo', 'Caracoles de Borgoña', 'Raclet de queso Courdavault', 'Camembert Pierrot', 'Sirope de arce', 'Tarta de azúcar', 'Sandwich de vegetales', 'Bollos de pan de Wimmer', 'Salsa de pimiento picante de Luisiana', 'Especias picantes de Luisiana', 'Cerveza Laughing Lumberjack', 'Barras de pan de Escocia', 'Queso Gudbrandsdals', 'Cerveza Outback', 'Crema de queso Fløtemys', 'Queso Mozzarella Giovanni', 'Caviar rojo', 'Queso de soja Longlife', 'Cerveza Klosterbier Rhönbräu', 'Licor Cloudberry', 'Salsa verde original Frankfurter'];

        for ($i=0; $i < 50; $i++) {

        	$cat = rand(1, 7); 
        	DB::table('productos')->insert([
	            'id_categoria' => $cat,
	            'nombre' => $productos[rand(0,76)],
	            'valor' => rand(1000, 10000),
	            'fecha_expiracion' => date('Y-m-d H:i:s', strtotime('+'.rand(10,30).' day', strtotime(date('Y-m-d H:i:s')))) ,
	        ]);
        }
    }
}
